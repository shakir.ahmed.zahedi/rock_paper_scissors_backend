package com.example.rockpaperscissors_backend.user;
import lombok.AllArgsConstructor;
import lombok.Data;
@Data
@AllArgsConstructor
public class UserEntity {
    String id;
    String name;
    String userName;
}
