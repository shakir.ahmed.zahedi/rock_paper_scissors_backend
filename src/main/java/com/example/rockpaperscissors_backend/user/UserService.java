package com.example.rockpaperscissors_backend.user;
import com.example.rockpaperscissors_backend.token.TokenEntity;
import com.example.rockpaperscissors_backend.token.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;
@Service
public class UserService {
    Map<String, UserEntity> users = new HashMap<>();
    @Autowired
    TokenRepository tokenRepository;

    public Optional<UserEntity> addUser(CreateUser createUser, TokenEntity token) {
        UserEntity userEntity = new UserEntity(
                UUID.randomUUID().toString(),
                createUser.getName(),
                createUser.getUserName());
        token.setName(userEntity.getName());
        tokenRepository.save(token);
        users.put(userEntity.getId(), userEntity);
        return Optional.of(userEntity);
    }

    public Stream<UserEntity> getAll() {
        return users.values().stream();
    }
}
