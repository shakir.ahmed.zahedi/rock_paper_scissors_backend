package com.example.rockpaperscissors_backend.user;
import com.example.rockpaperscissors_backend.exceptions.Activity;
import com.example.rockpaperscissors_backend.exceptions.ExceptionType;
import com.example.rockpaperscissors_backend.exceptions.UseException;
import com.example.rockpaperscissors_backend.token.TokenEntity;
import com.example.rockpaperscissors_backend.token.TokenRepository;
import com.example.rockpaperscissors_backend.token.TokenService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collector;
import java.util.stream.Collectors;
@RestController
@RequestMapping("/user")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")

public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    TokenService tokenService;

    @PostMapping("/name")
    public String setName(
            @RequestBody CreateUser createUser,
            @RequestHeader(value = "token", required = true) String tokenString) throws UseException {
        try {
            TokenEntity token = tokenService.getTokenByTokenString(tokenString)
                    .orElseThrow(() -> new UseException(Activity.ADD_NAME, ExceptionType.TOKEN_NOT_VALID));
            UserDTO userDTO = userService.addUser(createUser,token)
                   /* .map(userEntity -> {
                        token.setName(userEntity.getName());
                        return userEntity;
                    })*/
                    .map(this::toUserDTO)
                    .orElseThrow(() -> new UseException(Activity.ADD_NAME, ExceptionType.TOKEN_NOT_VALID));
            //tokenService.update(token);
            return userDTO.getName();
        }catch (Exception e){
            return "excep";
        }
    }

    private UserDTO toUserDTO(UserEntity userEntity) {
        return new UserDTO(
                userEntity.getId(),
                userEntity.getName(),
                userEntity.getUserName()
        );
    }
}
