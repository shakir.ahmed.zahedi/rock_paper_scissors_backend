package com.example.rockpaperscissors_backend.user;
import lombok.AllArgsConstructor;
import lombok.Value;
@Value
@AllArgsConstructor
public class CreateUser {
    String name;
    String userName;
}
