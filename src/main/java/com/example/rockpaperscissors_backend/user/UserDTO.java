package com.example.rockpaperscissors_backend.user;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
@Value
@Builder(toBuilder = true)
public class UserDTO {
    String id;
    String name;
    String userName;
}
