package com.example.rockpaperscissors_backend.exceptions;
public enum ExceptionType {
    TOKEN_NOT_VALID,
    GAME_NOT_FOUND,
    BAD_MOVE,
    USER_NOT_FOUND;
}
