package com.example.rockpaperscissors_backend.exceptions;
public class UseException extends Exception {
    Activity activity;
    ExceptionType exceptionType;


    public UseException(Activity activity, ExceptionType exceptionType) {
        super(activity+" failed because "+ exceptionType);
        this.activity = activity;
        this.exceptionType = exceptionType;

    }



    public Activity getActivity() {
        return activity;
    }

    public ExceptionType getExceptionType() {
        return exceptionType;
    }

}

