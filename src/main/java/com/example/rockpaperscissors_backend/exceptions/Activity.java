package com.example.rockpaperscissors_backend.exceptions;
public enum Activity {
    START_GAME,
    ALL_GAME,
    FIND_GAME,
    GAME_STATUS,
    JION_GAME,
    GAME_INFO,
    ADD_MOVE,
    ADD_NAME;
}
