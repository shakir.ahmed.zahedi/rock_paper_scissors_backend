package com.example.rockpaperscissors_backend.token;
import com.example.rockpaperscissors_backend.game.GameDTO;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;
@RestController
@RequestMapping("/auth")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TokenController {
    TokenService tokenService;

    @GetMapping("/token")
    public String createNewToken() {
        return tokenService.createNewToken()
                .getId();
    }
}
