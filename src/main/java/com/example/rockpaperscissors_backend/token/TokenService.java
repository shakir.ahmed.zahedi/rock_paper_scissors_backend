package com.example.rockpaperscissors_backend.token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
@Service
public class TokenService {
    @Autowired
    TokenRepository tokenRepository;

    public TokenEntity createNewToken() {
        TokenEntity token = new TokenEntity(UUID.randomUUID().toString(), "", "");
        tokenRepository.save(token);
        return token;
    }

    public Optional<TokenEntity> getTokenByTokenString(String tokenString) {
        return tokenRepository.findById(tokenString);
    }

    public TokenEntity update(TokenEntity token) {
        return tokenRepository.save(token);
    }
}
