package com.example.rockpaperscissors_backend.token;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "tokens")
public class TokenEntity {
    @Id
    String id;
    String name;
    String gameId;
}
