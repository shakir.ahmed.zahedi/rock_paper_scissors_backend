package com.example.rockpaperscissors_backend.token;
import org.springframework.data.repository.CrudRepository;
public interface TokenRepository extends CrudRepository<TokenEntity, String> {
}
