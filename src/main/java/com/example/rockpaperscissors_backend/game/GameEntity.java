package com.example.rockpaperscissors_backend.game;
import com.example.rockpaperscissors_backend.token.TokenEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "games")
public class GameEntity {
    @Id
    String id;
    @OneToOne
    TokenEntity token;
    String move;
    GameStatus game;
    @OneToOne
    TokenEntity opponentToken;
    String opponentMove;
}
