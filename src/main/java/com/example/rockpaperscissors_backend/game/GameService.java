package com.example.rockpaperscissors_backend.game;
import com.example.rockpaperscissors_backend.exceptions.Activity;
import com.example.rockpaperscissors_backend.exceptions.ExceptionType;
import com.example.rockpaperscissors_backend.exceptions.UseException;
import com.example.rockpaperscissors_backend.token.TokenEntity;
import com.example.rockpaperscissors_backend.token.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
@Service
public class GameService {
    @Autowired
    GameRepository gameRepository;
    @Autowired
    TokenRepository tokenRepository;

    public Optional<GameEntity> add(String tokenId) throws UseException {
        TokenEntity token = tokenRepository.findById(tokenId)
                .orElseThrow(() -> new UseException(
                        Activity.START_GAME, ExceptionType.TOKEN_NOT_VALID));
        GameEntity game = new GameEntity(
                UUID.randomUUID().toString(),
                token,
                "",
                GameStatus.OPEN,
                null,
                "");
        token.setGameId(game.getId());
        tokenRepository.save(token);
        gameRepository.save(game);
        return Optional.of(game);
    }
    public List<GameEntity> getAll(String tokenId) {
        return (List<GameEntity>) gameRepository.findAll();
    }
    /*public List<GameEntity> all() {
        return (List<GameEntity>) gameRepository.findAll();
                //.collect(Collectors.toList());
    }*/

    public Optional<GameEntity> getById(String gameId) {
        return gameRepository.findById(gameId);
    }

    public Optional<GameEntity> join(String gameId, String tokenId) throws UseException {
        GameEntity game = gameRepository.findById(gameId)
                .orElseThrow(() -> new UseException(Activity.JION_GAME, ExceptionType.GAME_NOT_FOUND));
        TokenEntity token = tokenRepository.findById(tokenId)
                .orElseThrow(() -> new UseException(Activity.JION_GAME, ExceptionType.TOKEN_NOT_VALID));
        game.setOpponentToken(token);
        game.setGame(GameStatus.ACTIVE);
        gameRepository.save(game);
        return Optional.of(game);
    }

    public Optional<GameEntity> addMove(String gameId, String sign) throws UseException {

        GameEntity game = gameRepository.findById(gameId)
                .orElseThrow(() -> new UseException(Activity.ADD_MOVE, ExceptionType.TOKEN_NOT_VALID));
        if (game.getToken() != null && game.getToken().getGameId() == gameId)
            game.setMove(sign.toUpperCase());
         if (game.getOpponentToken() != null && game.getOpponentToken().getGameId() == gameId)
            game.setOpponentMove(sign.toUpperCase());
        gameRepository.save(game);
        if (!game.getMove().isEmpty() && !game.getOpponentMove().isEmpty()) {
            game.setGame(gameResult(game.getMove(), game.getOpponentMove()));
            gameRepository.save(game);
        }
        return Optional.of(game);
    }

    private static GameStatus gameResult(String move, String opponentMove) {
        if (move.toUpperCase().equals(opponentMove.toUpperCase())) {
            return GameStatus.DRAW;
        }
        switch (move.toUpperCase()) {
            case "ROCK":
                return opponentMove.toUpperCase().equals("SCISSORS") ? GameStatus.WIN : GameStatus.LOSE;
            case "PAPER":
                return opponentMove.toUpperCase() == "ROCK" ? GameStatus.WIN : GameStatus.LOSE;
            case "SCISSORS":
                return opponentMove.toUpperCase().equals("PAPER") ? GameStatus.WIN : GameStatus.LOSE;
        }
        return null;
    }
}

