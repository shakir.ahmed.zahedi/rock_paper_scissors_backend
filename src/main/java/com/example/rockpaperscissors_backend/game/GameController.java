package com.example.rockpaperscissors_backend.game;
import com.example.rockpaperscissors_backend.exceptions.Activity;
import com.example.rockpaperscissors_backend.exceptions.ExceptionType;
import com.example.rockpaperscissors_backend.exceptions.UseException;
import com.example.rockpaperscissors_backend.token.TokenEntity;
import com.example.rockpaperscissors_backend.token.TokenService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.http.HttpResponse;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
@RestController
@RequestMapping("/games")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class GameController {
    @Autowired
    GameService gameService;
    @Autowired
    TokenService tokenService;

    @GetMapping()
    public List<GameDTO> allGames(@RequestHeader(value = "token", required = true) String tokenString) throws UseException {
        TokenEntity token = tokenService.getTokenByTokenString(tokenString)
                .orElseThrow(() -> new UseException(Activity.ALL_GAME, ExceptionType.TOKEN_NOT_VALID));
        return gameService.getAll(token.getId()).stream()
                .map(this::toGameDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("/start")
    public GameDTO addNewGame(
            @RequestHeader(value = "token", required = true) String tokenString)
            throws UseException {
        TokenEntity token = tokenService.getTokenByTokenString(tokenString)
                .orElseThrow(() -> new UseException(Activity.START_GAME, ExceptionType.TOKEN_NOT_VALID));
        System.out.println(token.getId());
        GameDTO gameDTO = gameService.add(token.getId())
                .map(this::toGameDTO)
                .orElseThrow(() -> new UseException(Activity.START_GAME, ExceptionType.TOKEN_NOT_VALID));
        return gameDTO;
    }

    @GetMapping("/status")
    public GameDTO gameStatus(
            @RequestHeader(value = "token", required = true) String tokenString)
            throws UseException {
        TokenEntity token = tokenService.getTokenByTokenString(tokenString)
                .orElseThrow(() -> new UseException(Activity.GAME_STATUS, ExceptionType.TOKEN_NOT_VALID));
        GameEntity game = gameService.getById(token.getGameId())
                .orElseThrow(() -> new UseException(Activity.GAME_STATUS, ExceptionType.GAME_NOT_FOUND));
        System.out.println("This is inside Status :" + game);
        return showStatus(game,token);
    }

    @GetMapping("/{id}")
    public GameDTO gameInfo(
            @RequestHeader(value = "token", required = true) String tokenString,
            @PathVariable(value = "id") String id)
            throws UseException {
        TokenEntity token = tokenService.getTokenByTokenString(tokenString)
                .orElseThrow(() -> new UseException(Activity.GAME_INFO, ExceptionType.TOKEN_NOT_VALID));
        GameEntity game = gameService.getById(id)
                .orElseThrow(() -> new UseException(Activity.GAME_INFO, ExceptionType.GAME_NOT_FOUND));
        System.out.println("This is inside Status :" + game);
        return showStatus(game,token);
    }

    @GetMapping("/join/{gameId}")
    public GameDTO joinAGame(
            @PathVariable(value = "gameId") String gameId,
            @RequestHeader(value = "token", required = true) String tokenString) throws UseException {
        TokenEntity token = tokenService.getTokenByTokenString(tokenString)
                .orElseThrow(() -> new UseException(Activity.JION_GAME, ExceptionType.TOKEN_NOT_VALID));
        token.setGameId(gameId);
        return gameService.join(gameId, token.getId())
                .map(this::toGameWithOpponentDTO)
                .orElseThrow(() -> new UseException(Activity.JION_GAME, ExceptionType.GAME_NOT_FOUND));
    }


    @GetMapping("/move/{sign}")
    public GameDTO addMove(
            @RequestHeader(value = "token", required = true) String tokenString,
            @PathVariable(value = "sign") String sign) throws UseException {
        TokenEntity token = tokenService.getTokenByTokenString(tokenString)
                .orElseThrow(() -> new UseException(Activity.ADD_MOVE, ExceptionType.TOKEN_NOT_VALID));
        GameEntity game = gameService.getById(token.getGameId())
                .orElseThrow(() -> new UseException(Activity.ADD_MOVE, ExceptionType.GAME_NOT_FOUND));
        System.out.println("Token" + game);
        gameService.addMove(token.getGameId(), sign)
                .orElseThrow(() -> new UseException(Activity.ADD_MOVE, ExceptionType.BAD_MOVE));
        return assignMove(game,token);
    }
    private GameDTO showStatus(GameEntity game,TokenEntity token) throws UseException {
        if (game.getOpponentToken() != null && game.getOpponentToken().getGameId() == game.getId())
            return toGameWithOpponentDTO(game);
        return gameService.getById(token.getGameId())
                .map(this::toGameDTO)
                .orElseThrow(() -> new UseException(Activity.GAME_STATUS, ExceptionType.GAME_NOT_FOUND));

    }
    private GameDTO assignMove(GameEntity game,TokenEntity token) {
        if (game.getOpponentToken() != null && game.getOpponentToken().getGameId() == game.getId())
            return toGameWithOpponentDTO(game);
        return toGameDTO(game);
    }
    private void swapGameStatus(GameEntity gameEntity)  {
        if (gameEntity.getOpponentToken() == null)
            gameEntity.setOpponentToken(new TokenEntity("", "", ""));
        if (gameEntity.getGame().toString() == GameStatus.LOSE.toString()){
            gameEntity.setGame(GameStatus.WIN);
            System.out.println("change lose to win");
        }
        else if (gameEntity.getGame().toString() == GameStatus.WIN.toString()){
            gameEntity.setGame(GameStatus.LOSE);
            System.out.println("change win to lose to win");
        }
        else
            System.out.println("Do nothing");
    }

    private GameDTO toGameDTO(GameEntity gameEntity) {
        if (gameEntity.getOpponentToken() == null)
            gameEntity.setOpponentToken(new TokenEntity("", "", ""));
        return new GameDTO(
                gameEntity.getId(),
                gameEntity.getToken().getName(),
                gameEntity.getMove(),
                gameEntity.getGame().toString(),
                gameEntity.getOpponentToken().getName(),
                gameEntity.getOpponentMove());
    }

    private GameDTO toGameWithOpponentDTO(GameEntity gameEntity){
        swapGameStatus(gameEntity);
        return new GameDTO(
                gameEntity.getId(),
                gameEntity.getOpponentToken().getName(),
                gameEntity.getOpponentMove(),
                gameEntity.getGame().toString(),
                gameEntity.getToken().getName(),
                gameEntity.getMove());
    }
}
