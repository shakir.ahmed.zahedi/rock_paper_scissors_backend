package com.example.rockpaperscissors_backend.game;
public enum GameMove {
    ROCK,
    PAPER,
    SCISSORS;
}
