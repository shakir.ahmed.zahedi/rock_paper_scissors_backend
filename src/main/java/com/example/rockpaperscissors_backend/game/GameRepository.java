package com.example.rockpaperscissors_backend.game;
import org.springframework.data.repository.CrudRepository;
public interface GameRepository extends CrudRepository<GameEntity,String> {
}
