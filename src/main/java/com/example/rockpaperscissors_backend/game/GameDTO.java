package com.example.rockpaperscissors_backend.game;
import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class GameDTO {
    String id;
    String name;
    String move;
    String game;
    String opponentName;
    String opponentMove;
}
