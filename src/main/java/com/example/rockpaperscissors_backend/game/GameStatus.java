package com.example.rockpaperscissors_backend.game;
public enum GameStatus {
    NONE,
    ACTIVE,
    OPEN,
    WIN,
    DRAW,
    LOSE;
}
