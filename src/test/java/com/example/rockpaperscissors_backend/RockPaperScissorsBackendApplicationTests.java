package com.example.rockpaperscissors_backend;
import com.example.rockpaperscissors_backend.exceptions.UseException;
import com.example.rockpaperscissors_backend.game.GameController;
import com.example.rockpaperscissors_backend.token.TokenController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
@SpringBootTest
@ActiveProfiles("integrationtest")
class RockPaperScissorsBackendApplicationTests {
    @Autowired
    TokenController tokenController;
    @Autowired
    GameController gameController;
    @Test
    void test() throws UseException {
        String newToken = tokenController.createNewToken();
        gameController.addNewGame(newToken);
    }
}

